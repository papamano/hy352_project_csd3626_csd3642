# HY-352: Software Engineering Project

## Information

This repository is for the project of the course HY-352: Software Engineering. It will be used as following:

* We will both have one **branch** named after our info for developing. These branches will be only for their "owner" and we are allowed to make any mistakes there.
* The master branch will contain the latest stable version of our project. Both of us are allowed to update the master branch with changes from other branches but only when we are **certain** that everything works as expected.
* The master branch will only contain, at any time, the **final** version of the project. Versions that arise during development will stay in their branches until they are finished. Therefore, the code that we will submit (TURNIN) will be the code in the master branch.
* We will use **tags** to indicate the process of development.

## Prerequisites

Required software:

* g++ v7.4.0

Required hardware:

* N/A

Required Files:

* *See Project Stucture*.

## Installation

To download project:
```bash
git clone https://gitlab.com/papamano/hy352_project_csd3626_csd3642.git
```

To build the library along with all test applications:
```bash
cd hy352_project_csd3626_csd3642/
make
```

## Usage

If you want to use our custom test application:
```bash
make test
```

If you want to use your own test application:
```bash
ApplicationName < testFile
```

If you want to run the application and play the game (duel) yourself:
```bash
./ApplicationName
```

More test applications are available in the **apps/** directory

## Project Structure
```
.
├── apps/
├── docs/
├── lib/
├── src/
├── unused/
├── tests/
├── .gitignore
├── Makefile
└── README.md
```

* apps: A collection of files used during development for testing.
* docs: Project documentation.
* lib: The created libraries.
* src: Source code.
* tests: A collection of tests files to be used as input for applications.
* unused: Snippets of code that is no longer used.

## Notes

* Please note that not all test files can be used for all applications. Each test file has been written targetting a specific application since the elements of the simulation (wizards, spells, etc) are different from one application to the other.

* Please note that the makefile is custom-made so it might fail under conditions that have not been tested. In such cases please build it by hand.

* Please run "$ **make clear**" before committing anything.

## Lines of Code

* 20 files
* 444 blank
* 108 comment
* 992 code
* 725 test code

*Library only. Does not include test code (files in **apps** directory).*

## Roadmap
See the open issues for a list of proposed features (and known issues).

## Contributors / Authors
* Papadogiannakis Emmanouil (csd3626@csd.uoc.gr)
* Dimitrakakis Efstathios (csd3642@csd.uoc.gr)

## Support

Please contact one of the project's contributors.

If you need an expert:
Anthony Savvidis (as@ics.forth.gr)

## License

Closed source. **Proprietary and confidential**.
Unauthorized copying of this project, via any medium is strictly prohibited.