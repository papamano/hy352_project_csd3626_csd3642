# Compiler
CC=g++

# Current directory
PROJECTDIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

# Project structure
ODIR=obj
SRCDIR=src
LIBDIR=lib
APPSDIR=apps

# Specify the include directories
INCLDIR=-I$(PROJECTDIR)$(SRCDIR)/

# Set compilation flags
# -Wshadow : Warn whenever a local variable or type declaration shadows another
#            variable, parameter, type or when a built-in function is shadowed
CFLAGS=-std=c++14 -Wshadow -Wall -Wold-style-cast -Wpedantic $(INCLDIR)

# Find all source files
SRCS:=$(shell cd $(SRCDIR); ls *.cpp)
# Get all object files by substituting .cpp with .o
OBJECTS=$(SRCS:%.cpp=%.o)

# Find all application source files
TESTS:=$(shell ls $(APPSDIR)/*.cpp)
# Get all executable files by substituting .cpp with .out
APPS=$(TESTS:%.cpp=%.out)

# Get object file paths
OBJ = $(patsubst %,$(ODIR)/%,$(OBJECTS))

# The library that will be created for the project
LIBRARY=libhogwarts.a

#################################### RULES ####################################

all: lib apps
	@echo 'Build successful'

# Create object files of source code
$(ODIR)/%.o: $(SRCDIR)/%.cpp
	@echo Compiling $*
	@mkdir -p $(ODIR)
	$(CC) $(CFLAGS) -c -o $@ $<

# Create the library of the projct
lib: $(OBJ)
	@echo 'Creating library'
	@mkdir -p $(LIBDIR)
	ar cr $(LIBDIR)/$(LIBRARY) $^

# Create executable of test code
$(APPSDIR)/%.out: $(APPSDIR)/%.cpp
	@echo Building $*
	$(CC) $(CFLAGS) $< $(LIBDIR)/$(LIBRARY) -o $@

# Create applications of project
apps: $(APPS)
	@echo 'Test applications created'

# Run a specific test application
test: lib apps
	@echo 'Running application'
	apps/custom.out < tests/simple.input

# Clean object files
clean:
	@rm -rf *.o
	@rm -rf $(ODIR)/*.o
	@rm -rf $(ODIR)
	@echo 'Object Files cleaned'

# Clean apps and lib
distclean:
	@rm -rf *.out
	@rm -rf $(APPSDIR)/*.out
	@echo 'Executables cleaned'
	@rm -rf $(LIBDIR)/*.a
	@echo 'Library cleaned'

# Clean the entire project
clear: clean distclean
	@echo 'Project directory is clean'
