#include <iostream>
#include <functional>
#include <vector>

typedef std::function<void(int)> Function;

typedef std::vector<Function> FVector;

int main(void) {

	FVector myFuncs;

	auto print = [](int num) { std::cout << num << std::endl; };
	auto foo = [](int num) { std::cout << "Foo " << num << std::endl; };
	auto bar = [](int num) { std::cout << "Bar " << num << std::endl; };

	myFuncs.push_back(print);
	myFuncs.push_back(foo);
	myFuncs.push_back(bar);

	for(auto & func : myFuncs) func(10);

	return 0;
}
