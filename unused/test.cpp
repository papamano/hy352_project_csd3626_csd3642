#include <iostream>

class Point {
public:
	virtual void print(void) = 0;
	virtual void move(int x, int y) = 0;
};

class Vector : public Point {
public:
	virtual void print(void) { std::cout << "print" << std::endl; }
	virtual void move(int x, int y) { std::cout << "move" << std::endl; }
};

void operator,(Point & a, Point & b) {
	std::cout << "Here" << std::endl;
}

void operator,(const Point & a, const Point & b) {
	std::cout << "There" << std::endl;
}

int main(void) {
	std::cout << "Hello world" << std::endl;

	Vector a;
	Vector b;

	a,b;

	Vector(),Vector();

	return 0;
}
