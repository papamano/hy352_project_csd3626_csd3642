#include <iostream>
#include <functional>
#include <string>

struct Name {
    std::string name;

    Name(const std::string & n) : name(n) { }
};



int main(void) {

    std::function<void(void)> func;

    func = []() { std::cout << "Hello world" << std::endl; };
    func();

    Name a("attacker");
    Name b("defender");

    func = [&a, &b]() { std::cout << a.name << " " << b.name << std::endl; };
    a.name = "Foobar";
    b.name = "Barfoo";
    func();
}