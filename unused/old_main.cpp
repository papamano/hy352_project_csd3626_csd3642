/* 
 * File:   main.cpp
 * Author: manos
 *
 * Created on December 23, 2019, 1:25 PM
 */

#include "Wizard.h"
#include "macros.h"

#include <iostream>

#include <memory>
#include <vector>
#include <map>
#include <cassert>

typedef std::shared_ptr<Wizard> WizardPtr;

typedef std::shared_ptr<std::vector<WizardPtr>> WizardVectorPtr;

typedef std::map<std::string, WizardPtr> WizardsWithName;

typedef std::pair<std::string, WizardPtr> WizardWithNamePair;

WizardsWithName CreatedWizards;

class WizardsCollection {
public:
    WizardVectorPtr elements;
    
    WizardsCollection(void) {
        elements = std::make_shared<std::vector<WizardPtr>>();
    }
    
    WizardsCollection & operator[] (const Wizard & wiz) {
        this->elements->push_back(WizardPtr(wiz.Clone()));
        assert(this->elements->size() >= 1);
        return *this;
    }
    
    WizardsCollection & operator[] (std::vector<WizardPtr> vec) {
        unsigned originalSize = vec.size();
        
        for(auto wiz : vec) {
            this->elements->push_back(wiz);
        }
        vec.clear();
        
        assert(this->elements->size() >= originalSize);
        return *this;
    }
    
    std::string toString(void) const {
        std::string s;
        for(auto i = elements->begin(); i != elements->end(); ++i) {
            s += i->get()->GetName() + "\n";
        }
        return s;
    }
};

std::vector<WizardPtr> operator , (const Wizard & a, const Wizard & b) {
    std::vector<WizardPtr> vec;
    
    vec.push_back(WizardPtr(a.Clone()));
    vec.push_back(WizardPtr(b.Clone()));
    
    assert(vec.size() == 2);
    return vec;
}

std::vector<WizardPtr> operator , (const Wizard & a, std::vector<WizardPtr> vec) {
    vec.push_back(WizardPtr(a.Clone()));
    assert(vec.size() >= 1);
    return vec;
}

std::vector<WizardPtr> operator , (std::vector<WizardPtr> vec, const Wizard & a) {
    vec.push_back(WizardPtr(a.Clone()));
    assert(vec.size() >= 1);
    return vec;
}

struct MapOfWizards {
    WizardsWithName elements;
    
    MapOfWizards(void) {
        
    }
    
    MapOfWizards & operator += (const Wizard & wiz) {
        this->elements.insert(WizardWithNamePair(wiz.GetName(), WizardPtr(wiz.Clone())));
        return *this;
        //this->elements->push_back(WizardPtr(wiz.Clone()));
    }

    MapOfWizards & operator += (const WizardsCollection & collection) {
        for(auto i = collection.elements->begin(); i != collection.elements->end(); ++i) {
            this->elements.insert( WizardWithNamePair( i->get()->GetName(), i->get()->Clone() ) );
        }
    }
    
    
    std::string toString(void) const {
        std::string s;
        for(auto i = elements.begin(); i != elements.end(); ++i) {
            s += i->second->GetName() + "\n";
        }
        return s;
    }
};


#ifdef FOO
MapOfWizards myWizards;

BEGIN_GAME

//lastDeclaration = Wizard {"Draco", "Slytherin", 100};
//CreatedWizards.insert(WizardWithNamePair(w.GetName(), w));

CREATE WIZARD {
    NAME: "Hermione Granger",
    HOUSE: "Gryffindor",
    HP: 100
};

CREATE WIZARDS[ 
    Wizard {
        NAME: "Draco", 
        HOUSE: "Slytherin",
        HP: 100
    },
    Wizard {
        NAME: "Harry Potter",
        HOUSE: "Gryffindor",
        HP: 150
    },
    Wizard {
        NAME: "Luna",
        HOUSE: "Ravenclaw",
        HP: 85
    }
];

std::cout << myWizards.toString() << std::endl;
std::cout << "Exciting" << std::endl;
END_GAME
#endif
