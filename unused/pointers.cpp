#include <iostream>
#include <memory>

struct Object {

    Object(void) { std::cout << "Object created" << std::endl; }

    virtual ~Object() { std::cout << "Object destroyed" << std::endl; }
};

typedef std::shared_ptr<Object> Pointer;

int main(void) {
    Pointer p(new Object());

    std::cout << "1" << std::endl;
    std::cout << "2" << std::endl;
    std::cout << "3" << std::endl;
    std::cout << "4" << std::endl;
    std::cout << "5" << std::endl;
    p.reset();
    std::cout << "6" << std::endl;
    std::cout << "7" << std::endl;
    std::cout << "8" << std::endl;
    std::cout << "9" << std::endl;
    std::cout << "10" << std::endl;

    return (EXIT_SUCCESS);
}