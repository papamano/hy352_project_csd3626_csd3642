#include <iostream>
#include <string>
#include <vector>

#ifdef TEST
#define SPELL(x) #x, ""

#define TO_STRING(x) #x
#define SPELL_NAME(x) TO_STRING(x), "testidion"

struct Foo {
    int a;

    Foo appendAndReturn(Foo & foo) {
        return foo;
    }
};

template<class T> T append(T a, T b) {
    return a.appendAndReturn(b);
}

template<> std::string append<std::string>(std::string a, std::string b) {
    return a.append(b);
}

#include <cassert>

std::vector<std::string> operator , (const std::string & a, const std::string & b) {
    std::vector<std::string> vector;

    if (!a.empty()) vector.push_back(a);
    if (!b.empty()) vector.push_back(b);

    return vector;
}

std::vector<std::string> operator , (std::vector<std::string> vector, const std::string & obj) {
    if (obj.empty()) return vector;
    vector.push_back(obj);
    assert(vector.size() >= 1);
    return vector;
}

std::vector<std::string> operator , (const std::string & obj, std::vector<std::string> vector) {
    if (obj.empty()) return vector;
    vector.push_back(obj);
    assert(vector.size() >= 1);
    return vector;
}
#endif

struct Temp {
    Temp operator[] (const Temp & t) { }

};

Temp operator , (const char * foo, const std::string & obj) {
    std::cout << "Case 0 Recognized " << obj << " and ignored " << foo << std::endl;;
    return Temp();
}

Temp operator , (const std::string & obj, const char * foo) {
    std::cout << "Case 1 Recognized " << obj << " and ignored " << foo << std::endl;;
    return Temp();
}

Temp operator , (const std::string & str, const Temp & t) {
    std::cout << "Case 2 Recognized " << str << std::endl;
    return t;
}

Temp operator , (const Temp & t, const std::string & str) {
    std::cout << "Case 3 Recognized " << str << std::endl;
    return t;
}

Temp operator , (const char * a, const Temp & t) { std::cout << "Case 1" << std::endl; return t; }
Temp operator , (const Temp & t, const char * a) { std::cout << "Case 2" << std::endl; return t; }

#define MY_SPELL(x) "before", std::string(#x), "after"

int main(void) {
    std::cout << "Hello world" << std::endl;

    // "before", string(Manos), "after" "before", string(Foo), "after" "before", string(Bar), "after"

    Temp temp = Temp()[
        MY_SPELL(Manos)
        MY_SPELL(FOO)
        MY_SPELL(BAR)
    ];
}