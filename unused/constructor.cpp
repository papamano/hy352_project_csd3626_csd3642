#include <iostream>

class Foo {

private:
	int i;

public:

	Foo(int x) : i(x) { }

};

int main(void) {

	Foo x{2};

}
