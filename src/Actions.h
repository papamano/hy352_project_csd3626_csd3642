/*
 * File:   Actions.h
 * Author: manos
 *
 * Created on December 23, 2019, 6:32 PM
 */

#ifndef ACTIONS_H
#define ACTIONS_H

#include "Wizard.h"
#include "Interfaces.h"
#include "ObjectAggregator.h"
#include "StringWrapper.h"

#include <string>

typedef std::function<void(void)> DurationActionFunc;

struct Wand {
    Wand & operator -- ();
    Wand & operator - ();
};

struct Action {
    Wizard * wizard = nullptr;
    int power;

    Action & operator , (Wizard & wiz);

    Action & operator , (const int num);

    virtual void Act(void);

    virtual ~Action();
};

struct HealAction : public Action {
    virtual void Act(void) override;
};

struct DamageAction : public Action {
    Wizard * attacker = nullptr;
    Wizard * defender = nullptr;
    unsigned currentRound = 0;

    DamageAction(Wizard & a, Wizard & d, unsigned round);

    virtual void Act(void) override;
};

struct EquipAction {
    Wizard * wizard = nullptr;

    EquipAction & operator , (Wizard & wiz);

    EquipAction & operator , (const int _unused_);

    EquipAction & operator , (const Wand & _unused_);
};

struct LearnAction {
    std::string name;
    LearningHandler * handler;

    LearnAction(LearningHandler & _handler, const std::string & _name);

    void operator += (ObjectAggregator<StringWrapper> collection);
};

struct DurationAction {
    unsigned rounds;

    DurationActionFunc action;

    ActionRegistrationHandler * handler = nullptr;

    DurationAction(void);

    DurationAction(ActionRegistrationHandler & _handler);

    void operator *= (const DurationAction & other);

    void operator += (const DurationAction & other);
};

DurationAction operator + (int num, DurationActionFunc func);

unsigned GetHealth(const Wizard & wiz, unsigned _unused_);

std::string GetHouse(const Wizard & wiz, unsigned _unused_);

std::string GetName(const Wizard & wiz, unsigned _unused_);

bool HasWand(const Wizard & wiz, unsigned _unused_);

template<typename X>
bool AndEvaluation(X op1) {
    return op1;
}

template<typename T, typename... Args>
bool AndEvaluation(T op1, Args... args) {
    return op1 && AndEvaluation(args...);
}

template<typename T, typename... Args>
bool EvalAnd(T op1, T op2, Args... args) {
    return op1 && AndEvaluation(op2, args...);
}

template<typename T>
bool OrEvaluation(T operand) {
    return operand;
}

template<typename T, typename... Args>
bool OrEvaluation(T op1, Args... args) {
    return op1 || OrEvaluation(args...);
}

template<typename T, typename... Args>
bool EvalOr(T op1, T op2, Args... args) {
    return op1 || OrEvaluation(op2, args...);
}

bool EvalNot(bool operand);

#endif /* ACTIONS_H */

