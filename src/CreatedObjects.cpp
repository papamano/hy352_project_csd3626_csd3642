/*
 * File:   CreatedObjects.cpp
 * Author: manos
 *
 * Created on December 23, 2019, 3:28 PM
 */

#include "CreatedObjects.h"

#include <cassert>
#include <future>

CreatedObjects::CreatedObjects() { }

bool CreatedObjects::IsValid(void) const {

    for(auto & wiz : wizards) {
        bool valid = !wiz.first.empty() && wiz.second && wiz.second->IsValid();
        if (!valid) return false;
    }

    for(auto & spell : spells) {
        bool valid = !spell.first.empty() && spell.second && spell.second->IsValid();
        if (!valid) return false;
    }

    return true;
}

void CreatedObjects::operator +=(const Wizard& wiz) {
    assert(wiz.IsValid());

    WizardWithNamePair pair(wiz.GetName(), WizardPtr(wiz.Clone()));
    wizards.insert(pair);

    assert(wizards.size() >= 1);
    assert(IsValid());
}

void CreatedObjects::operator +=(const ObjectAggregator<Wizard>& collection) {
    assert(collection.IsValid());

    for(auto i = collection.elements.begin(); i != collection.elements.end(); ++i) {
#ifdef NEW_ALLOCATION
        wizards.insert( WizardWithNamePair( i->get()->GetName(), WizardPtr(i->get()->Clone()) ) );
#else
        WizardPtr newPtr = *i;
        wizards.insert( WizardWithNamePair( i->get()->GetName(), newPtr ) );
#endif
    }

    assert(wizards.size() >= collection.elements.size());
    assert(IsValid());
}

void CreatedObjects::operator += (const Spell & spell) {
    assert(spell.IsValid());

    SpellWithNamePair pair(spell.GetName(), SpellPtr(spell.Clone()));
    spells.insert(pair);

    assert(spells.size() >= 1);
    assert(IsValid());
}

void CreatedObjects::operator += (const ObjectAggregator<Spell> & collection) {
    assert(collection.IsValid());

    for(auto i = collection.elements.begin(); i != collection.elements.end(); ++i) {
        SpellPtr newPtr = *i;
        spells.insert( SpellWithNamePair( i->get()->GetName(), newPtr ) );
    }

    assert(spells.size() >= collection.elements.size());
    assert(IsValid());
}

CreatedObjects::~CreatedObjects() {
    for(auto i = wizards.begin(); i != wizards.end(); ++i) i->second.reset();
    wizards.clear();
    assert(wizards.size() == 0);

    for(auto i = spells.begin(); i != spells.end(); ++i) i->second.reset();
    spells.clear();
    assert(spells.size() == 0);
}