/*
 * File:   Wizard.h
 * Author: manos
 *
 * Created on December 23, 2019, 1:11 PM
 */

#ifndef WIZARD_H
#define WIZARD_H

#include <string>
#include <vector>

class Wizard {

private:

    enum class HouseName {
        Gryffindor, Slytherin, Hufflepuff, Ravenclaw
    };

    std::string name;

    HouseName house;

    unsigned health;

    const unsigned maxHealth;

    bool hasWand;

    std::vector<std::string> learnedSpells;

    Wizard(const Wizard & wiz);

public:

    Wizard(const std::string & _name, const std::string & _house, unsigned _health);

    bool IsValid(void) const;

    std::string GetName(void) const;

    unsigned GetHealth(void) const;

    std::string GetHouse(void) const;

    unsigned GetMaxHealth(void) const;

    std::vector<std::string> GetSpells(void) const;

    bool HasWand(void) const;

    void EquipWand(void);

    void DropWand(void);

    void IncreaseHealth(unsigned num);

    void DecreaseHealth(unsigned num);

    void LearnSpell(const std::string & spell);

    Wizard * Clone(void) const;

    bool IsGryffindor(void) const;

    bool IsHufflepuff(void) const;

    bool IsRavenclaw(void) const;

    bool IsSlytherin(void) const;

    void SetFullHealth(void);

    void SetZeroHealth(void);

    bool KnowsSpell(const std::string & spell) const;

    bool KnowsSpells(void) const;

    static float GetAttackMultiplier(const Wizard & attacker, const Wizard & defender, const unsigned currentRound);

    static float GetDefenceMultiplier(const Wizard & attacker, const Wizard & defender);

    virtual ~Wizard();
};

#endif /* WIZARD_H */

