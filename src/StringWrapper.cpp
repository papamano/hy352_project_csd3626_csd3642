/*
 * File:   StringWrapper.cpp
 * Author: manos
 *
 * Created on December 24, 2019, 3:30 PM
 */

#include "StringWrapper.h"

#include <cassert>

StringWrapper::StringWrapper(const std::string & s) {
    assert(!s.empty());
    str = s;
}

bool StringWrapper::IsValid(void) const {
    return !str.empty();
}

StringWrapper * StringWrapper::Clone(void) const {
    return new StringWrapper(str);
}

StringWrapper::~StringWrapper() {
    str.clear();
}

StringWrapperPtrVector operator , (const char * ignored, const StringWrapper & obj) {
    assert(ignored);
    assert(obj.IsValid());

    StringWrapperPtrVector vector;

    vector.push_back( StringWrapperPtr(obj.Clone()) );

    assert(vector.size() == 1);
    assert(vector.at(0)->IsValid());
    assert(obj.IsValid());

    return vector;
}

StringWrapperPtrVector operator , (const StringWrapperPtrVector & vector, const char * ignored) {
    return vector;
}

StringWrapperPtrVector operator , (StringWrapperPtrVector & vector, const StringWrapper & obj) {
    assert(obj.IsValid());
    assert(vector.size() >= 1);

    vector.push_back( StringWrapperPtr(obj.Clone()) );

    assert(vector.size() >= 2);
    return vector;
}