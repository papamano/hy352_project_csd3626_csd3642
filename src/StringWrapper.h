/*
 * File:   StringWrapper.h
 * Author: manos
 *
 * Created on December 24, 2019, 3:30 PM
 */

#ifndef STRINGWRAPPER_H
#define STRINGWRAPPER_H

#include <string>
#include <vector>
#include <memory>

struct StringWrapper {
    std::string str;

    StringWrapper(const std::string & s);

    bool IsValid(void) const;

    StringWrapper * Clone(void) const;

    virtual ~StringWrapper();
};

typedef std::shared_ptr<StringWrapper> StringWrapperPtr;

typedef std::vector<StringWrapperPtr> StringWrapperPtrVector;

StringWrapperPtrVector operator , (const char * ignored, const StringWrapper & obj);

StringWrapperPtrVector operator , (const StringWrapperPtrVector & vector, const char * ignored);

StringWrapperPtrVector operator , (StringWrapperPtrVector & vector, const StringWrapper & obj);

#endif /* STRINGWRAPPER_H */

