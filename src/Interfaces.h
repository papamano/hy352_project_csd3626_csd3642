/*
 * File:   Interfaces.h
 * Author: manos
 *
 * Created on December 24, 2019, 12:48 PM
 */

#ifndef INTERFACES_H
#define INTERFACES_H

#include <functional>

#include "Wizard.h"

typedef std::function<void(void)> DurationActionFunc;

class LearningHandler {

public:
    virtual void LearnSpell(const std::string & wizard, const std::string & spell) = 0;

};

class ActionRegistrationHandler {

public:
    virtual void RegisterForAction(DurationActionFunc action, unsigned rounds) = 0;

    virtual void RegisterAfterAction(DurationActionFunc action, unsigned rounds) = 0;
};

#endif /* INTERFACES_H */

