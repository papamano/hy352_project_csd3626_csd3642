/*
 * File:   ObjectAggregator.h
 * Author: manos
 *
 * Created on December 23, 2019, 2:26 PM
 */

#ifndef OBJECTAGGREGATOR_H
#define OBJECTAGGREGATOR_H

#include <vector>
#include <memory>
#include <cassert>

template<class T> struct ObjectAggregator {

    typedef std::shared_ptr<T> ObjectPtr;

    std::vector<ObjectPtr> elements;

    ObjectAggregator(void) { }

    bool IsValid(void) const {
        for(auto obj : elements) {
            bool valid = obj && obj.get()->IsValid();
            if (!valid) return false;
        }
        return true;
    }

    ObjectAggregator & operator[] (const T & obj) {
        assert(obj.IsValid());

        elements.push_back(ObjectPtr(obj.Clone()));

        assert(elements.size() >= 1);
        assert(IsValid());

        return *this;
    }

    ObjectAggregator & operator[] (std::vector<ObjectPtr> vector) {
        unsigned originalSize = vector.size();

        for(auto obj : vector) {
            elements.push_back(obj);
        }
        vector.clear();

        assert(elements.size() >= originalSize);
        assert(IsValid());

        return *this;
    }

    virtual ~ObjectAggregator() {
        for(auto i = elements.begin(); i != elements.end(); ++i) i->reset();
        elements.clear();
    }
};

template<class T>
std::vector<std::shared_ptr<T>> operator , (const T & a, const T & b) {
    assert(a.IsValid());
    assert(b.IsValid());

    std::vector<std::shared_ptr<T>> vector;

    vector.push_back( std::shared_ptr<T>(a.Clone()) );
    vector.push_back( std::shared_ptr<T>(b.Clone()) );

    assert(vector.size() == 2);
    return vector;
}

template<class T>
std::vector<std::shared_ptr<T>> operator , (std::vector<std::shared_ptr<T>> vector, const T & obj) {
    assert(obj.IsValid());
    vector.push_back( std::shared_ptr<T>(obj.Clone()) );
    assert(vector.size() >= 1);
    return vector;
}

template<class T>
std::vector<std::shared_ptr<T>> operator , (const T & obj, std::vector<std::shared_ptr<T>> vector) {
    assert(obj.IsValid());
    vector.push_back( std::shared_ptr<T>(obj.Clone()) );
    assert(vector.size() >= 1);
    return vector;
}

#endif /* OBJECTAGGREGATOR_H */

