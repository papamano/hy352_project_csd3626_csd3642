/*
 * File:   Utilities.h
 * Author: manos
 *
 * Created on December 27, 2019, 8:32 PM
 */

#ifndef UTILITIES_H
#define UTILITIES_H

#include <string>

std::string & Trim(std::string & str);

std::string TrimNoModifying(const std::string & str);

bool IsOdd(unsigned i);

bool IsEven(unsigned i);

#endif /* UTILITIES_H */

