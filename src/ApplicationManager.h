/*
 * File:   ApplicationManager.h
 * Author: manos
 *
 * Created on December 24, 2019, 2:25 PM
 */

#ifndef APPLICATIONMANAGER_H
#define APPLICATIONMANAGER_H

#include "Interfaces.h"
#include "CreatedObjects.h"

#include <string>
#include <vector>
#include <map>

class ApplicationManager : public LearningHandler, public ActionRegistrationHandler, public CreatedObjects {
private:

    WizardPtr player1;
    WizardPtr player2;

    std::map<unsigned, std::vector<DurationActionFunc> > roundActions;

    unsigned currentRound = 1;

    void PrintWizardsList(void) const;

    void PrintSpellsList(const WizardPtr & wiz) const;

    void SelectPlayer(WizardPtr & wiz, const std::string & name);

    void PlayMove(WizardPtr & attacker, WizardPtr & defender, const std::string & name);

    bool CheckForDeaths(void) const;

    void ApplyDurationActions(void);

    void HealRavenclaw(WizardPtr & wiz);

public:
    virtual void LearnSpell(const std::string & wizard, const std::string & spell) override;

    virtual void RegisterForAction(DurationActionFunc action, unsigned rounds) override;

    virtual void RegisterAfterAction(DurationActionFunc action, unsigned rounds) override;

    void GameLoop(void);

    unsigned GetCurrentRound(void) const;

    virtual ~ApplicationManager();
};

#endif /* APPLICATIONMANAGER_H */

