/*
 * File:   Spell.cpp
 * Author: manos
 *
 * Created on December 23, 2019, 3:44 PM
 */

#include "Spell.h"

#include <string>
#include <functional>
#include <cassert>

Spell::Spell(std::string _name, SpellFunc _function) {
    assert(!_name.empty());
    assert(_function);

    name = _name;
    function = _function;
}

Spell::Spell(const Spell & spell) {
    name = spell.name;
    function = spell.function;

    assert(IsValid());
}

bool Spell::IsValid(void) const {
    return (
        !name.empty() &&
        function
    );
}

std::string Spell::GetName(void) const {
    return name;
}

void Spell::Act(Wizard & a, Wizard & b) const {
    function(a, b);
    assert(IsValid());
}

Spell * Spell::Clone(void) const {
    return new Spell(*this);
}

Spell::~Spell() {

}