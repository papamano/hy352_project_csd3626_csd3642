/*
 * File:   Wizard.cpp
 * Author: manos
 *
 * Created on December 23, 2019, 1:11 PM
 */

#include "Wizard.h"
#include "Utilities.h"

#include <string>
#include <algorithm>
#include <stdexcept>
#include <cassert>
#include <map>

Wizard::Wizard(const Wizard & wiz) : maxHealth(wiz.maxHealth) {
    assert(wiz.IsValid());

    health = wiz.health;
    name = wiz.name;
    house = wiz.house;
    hasWand = wiz.hasWand;
    learnedSpells = wiz.learnedSpells;

    assert(IsValid());
}

Wizard::Wizard(const std::string& _name, const std::string& _house, unsigned _health) : maxHealth(_health) {
    assert(!_name.empty());
    assert(!_house.empty());
    assert(_health > 0);

    name = _name;
    health = _health;
    hasWand = true;

    std::string houseName(_house);
    std::transform(_house.begin(), _house.end(), houseName.begin(), [](unsigned char c){ return std::tolower(c); });

    if (houseName == "gryffindor") house = Wizard::HouseName::Gryffindor;
    else if (houseName == "slytherin") house = Wizard::HouseName::Slytherin;
    else if (houseName == "hufflepuff") house = Wizard::HouseName::Hufflepuff;
    else if (houseName == "ravenclaw") house = Wizard::HouseName::Ravenclaw;
    else throw std::invalid_argument("Invalid house name for wizard \"" + name + "\"");

    assert(health == maxHealth);
    assert(IsValid());
}

bool Wizard::IsValid(void) const {

    for(auto spell : learnedSpells) {
        bool valid = !spell.empty();
        if (!valid) return false;
    }

    return (
        maxHealth >= health &&
        !name.empty() &&
        (house == HouseName::Gryffindor ||
         house == HouseName::Hufflepuff ||
         house == HouseName::Ravenclaw  ||
         house == HouseName::Slytherin)
    );
}

std::string Wizard::GetName(void) const {
    return name;
}

unsigned Wizard::GetHealth(void) const {
    return health;
}

std::string Wizard::GetHouse(void) const {
    switch(house) {
        case HouseName::Gryffindor: return "Gryffindor";
        case HouseName::Hufflepuff: return "Hufflepuff";
        case HouseName::Ravenclaw: return "Ravenclaw";
        case HouseName::Slytherin: return "Slytherin";
        default: assert(false);
    }
}

unsigned Wizard::GetMaxHealth(void) const {
    return maxHealth;
}

bool Wizard::HasWand(void) const {
    return hasWand;
}

std::vector<std::string> Wizard::GetSpells(void) const {
    return learnedSpells;
}

void Wizard::EquipWand(void) { hasWand = true; }

void Wizard::DropWand(void) { hasWand = false; }

void Wizard::IncreaseHealth(unsigned num) {
    unsigned original = health;
    health += num;
    assert(health >= original);
    assert(IsValid());
}

void Wizard::DecreaseHealth(unsigned num) {
    assert(health >= num);
    unsigned original = health;
    health -= num;
    assert(health <= original);
    assert(IsValid());
}

void Wizard::LearnSpell(const std::string & spell) {
    assert(!spell.empty());

    unsigned originalSize = learnedSpells.size();

    if (std::find(learnedSpells.begin(), learnedSpells.end(), spell) == learnedSpells.end()) learnedSpells.push_back(spell);

    assert(IsValid());
    assert(learnedSpells.size() >= originalSize);
}

Wizard * Wizard::Clone(void) const {
    return new Wizard(*this);
}

bool Wizard::IsGryffindor(void) const {
    return house == HouseName::Gryffindor;
}

bool Wizard::IsHufflepuff(void) const {
    return house == HouseName::Hufflepuff;
}

bool Wizard::IsRavenclaw(void) const {
    return house == HouseName::Ravenclaw;
}

bool Wizard::IsSlytherin(void) const {
    return house == HouseName::Slytherin;
}

void Wizard::SetFullHealth(void) {
    health = maxHealth;
    assert(IsValid());
}

void Wizard::SetZeroHealth(void) {
    health = 0;
    assert(IsValid());
}

bool Wizard::KnowsSpell(const std::string & spell) const {
    assert(!spell.empty());

    return std::find(learnedSpells.begin(), learnedSpells.end(), spell) != learnedSpells.end();
}

bool Wizard::KnowsSpells(void) const {
    return learnedSpells.size() != 0;
}

float Wizard::GetAttackMultiplier(const Wizard & attacker, const Wizard & defender, const unsigned currentRound) {
    assert(attacker.IsValid());
    assert(defender.IsValid());

    if (attacker.IsSlytherin() && defender.IsGryffindor()) return 1.2f;
    else if (attacker.IsSlytherin()) return 1.15f;

    if (attacker.IsHufflepuff()) return 1.07f;

    if (attacker.IsRavenclaw() &&
        IsOdd(currentRound)) return 1.07f;

    return 1.0f;
}

float Wizard::GetDefenceMultiplier(const Wizard & attacker, const Wizard & defender) {
    assert(attacker.IsValid());
    assert(defender.IsValid());

    if (defender.IsGryffindor() && attacker.IsSlytherin()) return 0.3f;
    else if (defender.IsGryffindor()) return 0.2f;

    if (defender.IsHufflepuff()) return 0.07f;

    return 0.0f;
}

Wizard::~Wizard() {
    name.clear();
    health = 0;
    learnedSpells.clear();
}