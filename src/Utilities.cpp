#include "Utilities.h"

#include <string>
#include <cctype>
#include <algorithm>

std::string & Trim(std::string & str) {
    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](int ch) {
        return !std::isspace(ch);
    }));

    str.erase(std::find_if(str.rbegin(), str.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), str.end());

    return str;
}

std::string TrimNoModifying(const std::string & str) {
    std::string result = str;
    return Trim(result);
}

bool IsOdd(unsigned i) { return (i % 2 != 0); }

bool IsEven(unsigned i) { return (i % 2 == 0); }