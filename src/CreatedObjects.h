/*
 * File:   CreatedObjects.h
 * Author: manos
 *
 * Created on December 23, 2019, 3:28 PM
 */

#ifndef CREATEDOBJECTS_H
#define CREATEDOBJECTS_H

#include "ObjectAggregator.h"
#include "Wizard.h"
#include "Spell.h"

#include <map>
#include <memory>

typedef std::shared_ptr<Wizard> WizardPtr;
typedef std::pair<std::string, WizardPtr> WizardWithNamePair;
typedef std::map<std::string, WizardPtr> WizardsMap;

typedef std::shared_ptr<Spell> SpellPtr;
typedef std::pair<std::string, SpellPtr> SpellWithNamePair;
typedef std::map<std::string, SpellPtr> SpellsMap;

class CreatedObjects {

protected:
    WizardsMap wizards;

    SpellsMap spells;

public:

    CreatedObjects(void);

    bool IsValid(void) const;

    void operator += (const Wizard & wiz);

    void operator += (const ObjectAggregator<Wizard> & collection);

    void operator += (const Spell & spell);

    void operator += (const ObjectAggregator<Spell> & collection);

    virtual ~CreatedObjects();

};

#endif /* CREATEDOBJECTS_H */

