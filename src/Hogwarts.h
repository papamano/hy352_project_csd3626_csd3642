/*
 * File:   Hogwarts.h
 * Author: manos
 *
 * Created on December 27, 2019, 1:13 PM
 */

#ifndef HOGWARTS_H
#define HOGWARTS_H

#include "Wizard.h"
#include "Spell.h"
#include "Actions.h"
#include "ApplicationManager.h"
#include "globals.h"
#include "macros.h"

#include <iostream>
#include <exception>

#endif /* HOGWARTS_H */

