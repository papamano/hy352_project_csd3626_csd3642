/*
 * File:   macros.h
 * Author: manos
 *
 * Created on December 23, 2019, 1:38 PM
 */

#ifndef MACROS_H
#define MACROS_H

#define BEGIN_GAME int main(void) { try {

#define END_GAME ; } catch (const std::exception & e) { std::cerr << "There was an error: " << e.what() << std::endl; } return EXIT_SUCCESS; }

#define NAME (false) ? ""
#define HOUSE (false) ? ""
#define HP (false) ? 0
#define ACTION (false) ? SpellFunc()

#define START [](Wizard & attacker, Wizard & defender) {
#define END ;}

#define WIZARDS ObjectAggregator<Wizard>()

#define WIZARD Wizard

#define SPELLS ObjectAggregator<Spell>()

#define SPELL Spell

#define CREATE ;manager +=

#define SHOW ;std::cout <<

#define ATTACKER attacker,
#define DEFENDER defender,

#define GET_HP(WIZ) GetHealth(WIZ 0)
#define GET_HOUSE(WIZ) GetHouse(WIZ 0)
#define GET_NAME(WIZ) GetName(WIZ 0)
#define HAS_WAND(WIZ) HasWand(WIZ 0)

#define DAMAGE ;DamageAction(attacker, defender, manager.GetCurrentRound()),
#define HEAL ;HealAction(),
#define EQUIP ;EquipAction(),

#define AND EvalAnd
#define OR EvalOr
#define NOT EvalNot

#define IF ;if(
#define DO ){

#define ELSE_IF ;} else if (
#define ELSE ;} else {

#define FOR ;DurationAction(manager) *=
#define AFTER ;DurationAction(manager) +=

#define ROUNDS + [&attacker, &defender](void

#define SPELL_NAME(x) "unused_before", StringWrapper(#x), "unused_after"

#define LEARN ) += ObjectAggregator<StringWrapper>()

#define MR ;LearnAction(manager,

#define MS MR
#define MRS MR

#define DUEL ;manager.GameLoop();

#endif /* MACROS_H */

