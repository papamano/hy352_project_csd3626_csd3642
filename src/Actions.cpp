/*
 * File:   Actions.cpp
 * Author: manos
 *
 * Created on December 23, 2019, 6:32 PM
 */

#include "Actions.h"

#include <cassert>
#include <cmath> /* For round */

Wand & Wand::operator --() { return *this; }

Wand & Wand::operator -() { return *this; }

Action & Action::operator , (Wizard & wiz) {
    assert(wiz.IsValid());
    wizard = &wiz;
    return *this;
}

Action & Action::operator , (const int num) {
    assert(num > 0);
    power = num;

    this->Act();

    return *this;
}

void Action::Act(void) { assert(false); }

Action::~Action() {
    wizard = nullptr;
    power = 0;
}

void HealAction::Act(void) {
    assert(power >= 0);
    assert(wizard && wizard->IsValid());

    if (wizard->GetHealth() + power < wizard->GetMaxHealth()) wizard->IncreaseHealth(power);
    else wizard->SetFullHealth();

    assert(wizard->IsValid());
}

DamageAction::DamageAction(Wizard & a, Wizard & d, unsigned round) {
    assert(a.IsValid());
    assert(d.IsValid());

    attacker = &a;
    defender = &d;
    currentRound = round;

    assert(attacker && attacker->IsValid());
    assert(defender && defender->IsValid());
}

void DamageAction::Act(void) {
    assert(power >= 0);
    assert(wizard && wizard->IsValid());
    assert(attacker && attacker->IsValid());
    assert(defender && defender->IsValid());

    unsigned attack  = static_cast<unsigned>( round(power * Wizard::GetAttackMultiplier(*attacker, *wizard, currentRound)) );
    unsigned defence = static_cast<unsigned>( round(power * Wizard::GetDefenceMultiplier(*attacker, *wizard)) );

    unsigned damage = attack - defence;
    assert(damage <= attack);

    if(wizard->GetHealth() > damage) wizard->DecreaseHealth(damage);
    else wizard->SetZeroHealth();

    assert(wizard && wizard->IsValid());
}

EquipAction & EquipAction::operator , (Wizard & wiz) {
    assert(wiz.IsValid());
    wizard = &wiz;
    return *this;
}

EquipAction & EquipAction::operator , (const int _unused_) {
    assert(wizard && wizard->IsValid());
    wizard->DropWand();
    assert(!wizard->HasWand());
    return *this;
}

EquipAction & EquipAction::operator , (const Wand & _unused_) {
    assert(wizard && wizard->IsValid());
    wizard->EquipWand();
    assert(wizard->HasWand());
    return *this;
}

LearnAction::LearnAction(LearningHandler & _handler, const std::string & _name) {
    assert(!_name.empty());
    name = _name;
    handler = &_handler;
}

void LearnAction::operator += (ObjectAggregator<StringWrapper> collection) {
    assert(collection.IsValid());
    assert(handler);

    for(auto & spell : collection.elements) handler->LearnSpell(name, spell.get()->str);
}

DurationAction::DurationAction(void) { }

DurationAction::DurationAction(ActionRegistrationHandler & _handler) {
    handler = &_handler;
}

void DurationAction::operator *= (const DurationAction & other) {
    assert(other.action);
    assert(handler);

    action = other.action;
    rounds = other.rounds;
    handler->RegisterForAction(action, rounds);
}

void DurationAction::operator += (const DurationAction & other) {
    assert(other.action);
    assert(handler);

    action = other.action;
    rounds = other.rounds;
    handler->RegisterAfterAction(action, rounds);
}

DurationAction operator + (int num, DurationActionFunc func) {
    assert(func);
    assert(num >= 0);

    DurationAction action;
    action.rounds = num;
    action.action = func;
    return action;
}

unsigned GetHealth(const Wizard & wiz, unsigned _unused_) {
    return wiz.GetHealth();
}

std::string GetHouse(const Wizard & wiz, unsigned _unused_) {
    return wiz.GetHouse();
}

std::string GetName(const Wizard & wiz, unsigned _unused_) {
    return wiz.GetName();
}

bool HasWand(const Wizard & wiz, unsigned _unused_) {
    return wiz.HasWand();
}

bool EvalNot(bool operand) {
    return !operand;
}