/*
 * File:   ApplicationManager.cpp
 * Author: manos
 *
 * Created on December 24, 2019, 2:25 PM
 */

#include "ApplicationManager.h"
#include "Utilities.h"

#include <iostream>
#include <cctype>
#include <algorithm>

namespace Display {

    void PrintSmallLine(void) {
        std::cout << "--------------------------------" << std::endl;
    }

    void PrintLargeLine(void) {
        std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
    }

    void PrintMediumLine(void) {
        std::cout << "#####################################" << std::endl;
    }

    void PrintWizardInfo(const WizardPtr & wiz) {
        assert(wiz && wiz->IsValid());

        std::cout << std::endl;
        PrintMediumLine();
        std::cout << "Name: " << wiz->GetName() << std::endl;
        std::cout << "HP: " << wiz->GetHealth() << std::endl;
        std::cout << "House: " << wiz->GetHouse() << std::endl;
        std::cout << "Wand" << (wiz->HasWand() ? " " : " not ") << "equipped" << std::endl;
        PrintMediumLine();
        std::cout << std::endl;
    }

    void PrintRound(unsigned currentRound) {
        PrintLargeLine();
        std::cout << "Round " << currentRound << std::endl;
        PrintLargeLine();
    }
}

void ApplicationManager::PrintWizardsList(void) const {
    Display::PrintSmallLine();
    for(auto wiz : wizards) std::cout << wiz.first << std::endl;
    Display::PrintSmallLine();
}

void ApplicationManager::PrintSpellsList(const WizardPtr & wiz) const {
    assert(wiz && wiz->IsValid());

    Display::PrintSmallLine();
    auto spellsList = wiz->GetSpells();
    for(auto spell : spellsList) std::cout << spell << std::endl;
    Display::PrintSmallLine();
}

void ApplicationManager::SelectPlayer(WizardPtr & wiz, const std::string & name) {
    assert(!name.empty());

    std::string input;

    while(true) {
        std::cout << std::endl << name  << " select a wizard:" << std::endl;

        PrintWizardsList();
        std::getline(std::cin, input);

        auto wizardObj = wizards.find(Trim(input));
        if (wizardObj != wizards.end()) {

            assert(wizardObj->second && wizardObj->second->IsValid());

            wiz.reset(wizardObj->second->Clone());
            break;
        } else {
            std::cerr << "\nInvalid input. Wizard does not exist" << std::endl;
        }
    }

    assert(wiz && wiz->IsValid());
}

void ApplicationManager::PlayMove(WizardPtr & attacker, WizardPtr & defender, const std::string & name) {
    assert(attacker && attacker->IsValid());
    assert(defender && defender->IsValid());
    assert(!name.empty());

    std::string input;

    if (!attacker->HasWand()) {
        std::cout << attacker->GetName() << " (" << name << ") has not a wand so he can't cast a spell.\n" << std::endl;
        return;
    }

    if (attacker->GetSpells().size() == 0) {
        std::cout << attacker->GetName() << " (" << name << ") cannot play because they haven't learned any spells.\n" << std::endl;
        return;
    }

    while(true) {
        std::cout << attacker->GetName() << " (" << name << ") select a spell:" << std::endl;
        PrintSpellsList(attacker);
        std::getline(std::cin, input);
        input = Trim(input);

        if (!attacker->KnowsSpell(input)) {
            std::cerr << "\nInvalid input. Wizard does not know such spell." << std::endl;
            continue;
        }

        auto spellObj = spells.find(input);
        if (spellObj != spells.end()) {
            spellObj->second->Act(*attacker, *defender);
#ifdef PRINT_ATTACKER_FIRST
            Display::PrintWizardInfo(attacker);
            Display::PrintWizardInfo(defender);
#else
            Display::PrintWizardInfo(player1);
            Display::PrintWizardInfo(player2);
#endif
            break;
        } else {
            std::cerr << "\nInvalid input. Spell does not exist" << std::endl;
        }
    }

    assert(attacker && attacker->IsValid());
    assert(defender && defender->IsValid());
}

bool ApplicationManager::CheckForDeaths(void) const {
    assert(player1 && player1->IsValid());
    assert(player2 && player2->IsValid());

    if (player1->GetHealth() == 0) {
        std::cout << player1->GetName() << " (Player1) is dead.\n" << player2->GetName() << " (Player2) wins!!!" << std::endl;
        return true;
    } else if (player2->GetHealth() == 0) {
        std::cout << player2->GetName() << " (Player2) is dead.\n" << player1->GetName() << " (Player1) wins!!!" << std::endl;
        return true;
    }
    return false;
}

void ApplicationManager::ApplyDurationActions(void) {
    auto iterator = roundActions.find(currentRound);

    if (iterator == roundActions.end()) return;

    auto vector = iterator->second;

    for(auto & func : vector) func();
}

void ApplicationManager::HealRavenclaw(WizardPtr & wiz) {
    assert(wiz && wiz->IsValid());
    assert(wiz->IsRavenclaw());

    if (IsEven(currentRound)) {
        unsigned heal = static_cast<unsigned>( 0.05 * wiz->GetMaxHealth() );

        if (wiz->GetHealth() + heal >= wiz->GetMaxHealth()) wiz->SetFullHealth();
        else wiz->IncreaseHealth(heal);

#define PRINT_BACKGROUND_ACTIONS
#ifdef PRINT_BACKGROUND_ACTIONS
        std::cout << "Ravenclaw wizard \"" << wiz->GetName() << "\" is automatically healed." << std::endl;
#endif

        assert(wiz->IsValid());
    }
}

void ApplicationManager::LearnSpell(const std::string & wizard, const std::string & spell) {
    assert(!wizard.empty());
    assert(!spell.empty());

    auto wizardObj = wizards.find(wizard);
    if (wizardObj == wizards.end())
        throw std::logic_error("Wizard " + wizard + " does not exist");

    if (spells.find(spell) == spells.end())
        throw std::logic_error("Spell " + spell + " does not exist");

    auto wiz = wizardObj->second;
    assert(wiz && wiz->IsValid());

    wiz->LearnSpell(spell);

    assert(wiz && wiz->IsValid());
}

void ApplicationManager::RegisterForAction(DurationActionFunc action, unsigned rounds) {
    assert(action);

    for(register unsigned i = 1; i < rounds; ++i) {
        RegisterAfterAction(action, i);
    }
    action();
}

void ApplicationManager::RegisterAfterAction(DurationActionFunc action, unsigned rounds) {
    assert(action);

    unsigned targetRound = currentRound + rounds;

    if (roundActions.find(targetRound) != roundActions.end()) {
        roundActions.at(targetRound).push_back(action);
    } else {
        std::vector<DurationActionFunc> vector;
        vector.push_back(action);
        roundActions[targetRound] = vector;
    }
}

void ApplicationManager::GameLoop(void) {
    SelectPlayer(player1, "Player1");
    SelectPlayer(player2, "Player2");

    assert(player1 && player1->IsValid());
    assert(player2 && player2->IsValid());

    std::cout << "\nPlayer1 is \"" << player1->GetName() << "\"" << std::endl;
    std::cout << "Player2 is \"" << player2->GetName() << "\"" << std::endl;
    std::cout << "Let there be war!\n" << std::endl;

    if (!player1->KnowsSpells() && !player2->KnowsSpells()) {
        std::cout << "Wizards do not know any spells. They cannot duel." << std::endl;
        return;
    }

    Display::PrintWizardInfo(player1);
    Display::PrintWizardInfo(player2);

    while(true) {
        Display::PrintRound(currentRound);
        if (player1->IsRavenclaw()) HealRavenclaw(player1);
        if (player2->IsRavenclaw()) HealRavenclaw(player2);

        ApplyDurationActions();
        if (CheckForDeaths()) break;

        PlayMove(player1, player2, "Player1");
        if (CheckForDeaths()) break;

        PlayMove(player2, player1, "Player2");
        if (CheckForDeaths()) break;

        currentRound += 1;
    }
}

unsigned ApplicationManager::GetCurrentRound(void) const {
    return currentRound;
}

ApplicationManager::~ApplicationManager() {
    player1.reset();
    player2.reset();

    for(auto & pair : roundActions) {
        for(auto & func : pair.second) func = nullptr;
        pair.second.clear();
    }

    currentRound = 0;
}