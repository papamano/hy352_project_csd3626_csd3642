/*
 * File:   Spell.h
 * Author: manos
 *
 * Created on December 23, 2019, 3:44 PM
 */

#ifndef SPELL_H
#define SPELL_H

#include "Wizard.h"

#include <string>
#include <functional>

typedef std::function<void(Wizard &,Wizard &)> SpellFunc;

class Spell {
private:

    std::string name;

    SpellFunc function;

    Spell(const Spell & spell);

public:

    Spell(std::string _name, SpellFunc _function);

    bool IsValid(void) const;

    std::string GetName(void) const;

    void Act(Wizard & a, Wizard & b) const;

    Spell * Clone(void) const;

    virtual ~Spell();
private:

};

#endif /* SPELL_H */

