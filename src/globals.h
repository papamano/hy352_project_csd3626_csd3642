/*
 * File:   globals.h
 * Author: manos
 *
 * Created on December 27, 2019, 1:14 PM
 */

#ifndef GLOBALS_H
#define GLOBALS_H

#include "ApplicationManager.h"
#include "Actions.h"

#ifdef GREEEK_ALPHA_SUPPORTED
extern Wand α;
#endif

extern Wand o;
extern int _;
extern ApplicationManager manager;

#endif /* GLOBALS_H */

