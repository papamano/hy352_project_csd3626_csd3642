/*
 * File:   battle.cpp
 * Author: manos
 *
 * Created on December 27, 2019, 8:06 PM
 */

#include "Hogwarts.h"

BEGIN_GAME

    CREATE SPELL {
        NAME: "SelfHarm",
        ACTION: START
            DAMAGE ATTACKER 10
        END
    }

    CREATE SPELL {
        NAME: "Ifs",
        ACTION: START
            IF GET_HP(ATTACKER) >= 10 DO
                IF GET_HP(ATTACKER) >= 20 DO
                    IF GET_HP(ATTACKER) >= 30 DO
                        IF GET_HP(ATTACKER) >= 40 DO
                            DAMAGE DEFENDER 20
                        END
                    END
                END
            END
        END
    }

    CREATE SPELL {
        NAME: "Fors",
        ACTION: START
            FOR 5 ROUNDS DO
                FOR 5 ROUNDS DO
                    DAMAGE DEFENDER 5
                END
            END
        END
    }

    CREATE SPELLS [
        SPELL {
            NAME: "Pass",
            ACTION: START
            END
        },
        SPELL {
            NAME: "Afters",
            ACTION: START
            EQUIP ATTACKER _
            EQUIP DEFENDER _
            AFTER 2 ROUNDS DO
                AFTER 2 ROUNDS DO
                    /*TODO: Explain why "DAMAGE 10" fails at runtime */
                    DAMAGE DEFENDER 1000
                    EQUIP ATTACKER ---o
                END
            END
            END
        }
    ]

    CREATE WIZARD {
        NAME: "Harry",
        HOUSE: "Gryffindor",
        HP: 100
    }

    CREATE WIZARD {
        NAME: "Draco",
        HOUSE: "Slytherin",
        HP: 90
    }

    CREATE WIZARDS [
        WIZARD {
            NAME: "Cedric",
            HOUSE: "Hufflepuff",
            HP: 100
        },
        WIZARD {
            NAME: "Cho",
            HOUSE: "Ravenclaw",
            HP: 100
        }
    ]

    MR "Harry" LEARN [
        SPELL_NAME(SelfHarm)
        SPELL_NAME(Ifs)
        SPELL_NAME(Fors)
        SPELL_NAME(Afters)
        SPELL_NAME(Pass)
    ]

    MR "Draco" LEARN [
        SPELL_NAME(Pass)
    ]

    MS "Cho" LEARN [
        SPELL_NAME(SelfHarm)
    ]

    MR "Cedric" LEARN [
        SPELL_NAME(SelfHarm)
    ]

    DUEL

END_GAME