/*
 * File:   main.cpp
 * Author: manos
 *
 * Created on December 23, 2019, 2:39 PM
 */

#include "Hogwarts.h"

BEGIN_GAME

CREATE WIZARD {
    NAME: "Hermione Granger",
    HOUSE: "Gryffindor",
    HP: 100
}

CREATE WIZARDS[
    WIZARD {
        NAME: "Draco",
        HOUSE: "Slytherin",
        HP: 100
    },
    WIZARD {
        NAME: "Harry Potter",
        HOUSE: "Gryffindor",
        HP: 150
    },
    WIZARD {
        NAME: "Luna",
        HOUSE: "Ravenclaw",
        HP: 85
    }
]


CREATE SPELL {
   NAME: "Levicorpus",
   ACTION: START
   END
}

CREATE SPELL {
   NAME: "Pass",
   ACTION: START
   END
}

CREATE SPELLS [
    SPELL {
        NAME: "Leviosa",
        ACTION: START
            SHOW GET_HP(ATTACKER) << std::endl
            SHOW GET_NAME(DEFENDER) << std::endl
            SHOW GET_HOUSE(DEFENDER) << std::endl
            EQUIP ATTACKER _
            SHOW HAS_WAND(ATTACKER) << std::endl
            EQUIP ATTACKER ---o
            SHOW HAS_WAND(ATTACKER) << std::endl
            DAMAGE ATTACKER 20
            DAMAGE DEFENDER 20
            HEAL DEFENDER 30
            HEAL ATTACKER 80
            SHOW AND(GET_HOUSE(ATTACKER) == "Gryffindor", GET_HP(ATTACKER) > 20) << std::endl
            SHOW OR( GET_HOUSE(DEFENDER) == "Slytherin", NOT (GET_HOUSE(ATTACKER) == "Ravenclaw"), GET_HP(DEFENDER) <= 20 ) << std::endl
            SHOW NOT (AND (GET_HP(DEFENDER) > 20, GET_HP(DEFENDER) < 70)) << std::endl

            IF GET_HP(DEFENDER) <= 20 DO
                DAMAGE DEFENDER 10
            ELSE_IF GET_HP(DEFENDER) <= 50 DO
                DAMAGE DEFENDER 20
            ELSE
                DAMAGE DEFENDER 30
            END

            FOR 5 ROUNDS DO
                DAMAGE ATTACKER 20
            END

            AFTER 20 ROUNDS DO
                HEAL DEFENDER 30
                SHOW GET_HP(ATTACKER)
            END
        END
    },

    SPELL {
        NAME: "Anapneo",
        ACTION: START
            DAMAGE DEFENDER 20
        END
    },

    SPELL {
        NAME: "Expulso",
        ACTION: START
            FOR 2 ROUNDS DO
                DAMAGE DEFENDER 10
            END
        END
    }
]

CREATE SPELL {
    NAME: "Expelliarmus",
    ACTION: START
        EQUIP DEFENDER _
        AFTER 2 ROUNDS DO
            EQUIP DEFENDER ---o
        END
    END
}

CREATE SPELL {
    NAME: "NUKE_TOWN",
    ACTION: START
        EQUIP DEFENDER _
        EQUIP ATTACKER _
        AFTER 5 ROUNDS DO
            DAMAGE DEFENDER 1000000
            DAMAGE ATTACKER 1000000
        END
    END
}

MR "Draco" LEARN [
    SPELL_NAME(Anapneo)
    SPELL_NAME(Expulso)
    SPELL_NAME(Levicorpus)
    SPELL_NAME(Pass)
]

MS "Luna" LEARN [
    SPELL_NAME(Expulso)
    SPELL_NAME(Expelliarmus)
    SPELL_NAME(Pass)
]

MR "Harry Potter" LEARN [
    SPELL_NAME(NUKE_TOWN)
]

DUEL
END_GAME

