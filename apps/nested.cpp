#include "Hogwarts.h"

BEGIN_GAME

CREATE WIZARD {
    NAME: "Harry",
    HOUSE: "Gryffindor",
    HP: 100
}

CREATE WIZARDS [
    WIZARD {
        NAME: "Luna",
        HOUSE: "Ravenclaw",
        HP: 100
    }
]

CREATE WIZARDS [
    WIZARD {
        NAME: "Draco",
        HOUSE: "Slytherin",
        HP: 100
    },
    WIZARD {
        NAME: "Cedric",
        HOUSE: "Hufflepuff",
        HP: 100
    },
    WIZARD {
        NAME: "Albus Dumbledore",
        HOUSE: "Gryffindor",
        HP: 120
    }
]

CREATE SPELL {
    NAME: "IfInFor",
    ACTION: START
        FOR 2 ROUNDS DO
            IF GET_HOUSE(ATTACKER) == "Gryffindor" DO
                DAMAGE ATTACKER 10
            ELSE_IF GET_HOUSE(ATTACKER) == "Slytherin" DO
                DAMAGE DEFENDER 15
            ELSE
                DAMAGE DEFENDER 20
            END
        END
    END
}

CREATE SPELLS [
    SPELL {
        NAME: "IfInIf",
        ACTION: START
            IF GET_HP(ATTACKER) >= 10 DO
                IF GET_HP(ATTACKER) >= 20 DO
                    IF GET_HP(ATTACKER) >= 30 DO
                        IF GET_HP(ATTACKER) >= 40 DO
                            DAMAGE DEFENDER 20
                        END
                    END
                END
            END
        END
    }
]

CREATE SPELLS [
    SPELL {
        NAME: "IfInAfter",
        ACTION: START
            AFTER 2 ROUNDS DO
                IF GET_HP(DEFENDER) > 50 DO
                    DAMAGE DEFENDER 50
                ELSE
                    DAMAGE DEFENDER 20
                END
            END
        END
    },
    SPELL {
        NAME: "ForInIf",
        ACTION: START
            IF GET_NAME(ATTACKER) == "Harry" DO
                FOR 3 ROUNDS DO
                    DAMAGE DEFENDER 10
                END
            END
        END
    },
    SPELL {
        NAME: "AfterInIf",
        ACTION: START
            IF GET_NAME(DEFENDER) != "Draco" DO
                AFTER 3 ROUNDS DO
                    DAMAGE DEFENDER 10
                END
            END
        END
    },
    SPELL {
        NAME: "Multiple",
        ACTION: START
            AFTER 2 ROUNDS DO
                EQUIP DEFENDER _
            END
            AFTER 3 ROUNDS DO
                DAMAGE DEFENDER 10
            END
            AFTER 4 ROUNDS DO
                DAMAGE DEFENDER 20
            END
            AFTER 5 ROUNDS DO
                DAMAGE DEFENDER 30
            END
            FOR 5 ROUNDS DO
                DAMAGE ATTACKER 2
            END
        END
    },
    SPELL {
        NAME: "Pass",
        ACTION: START
        END
    },
    SPELL {
        NAME: "Jab",
        ACTION: START
            DAMAGE DEFENDER 10
        END
    },
    SPELL {
        NAME: "Anapneo",
        ACTION: START
            HEAL ATTACKER 10
        END
    },
    SPELL {
        NAME: "Nuke",
        ACTION: START
            EQUIP ATTACKER _
            EQUIP DEFENDER _
            DAMAGE DEFENDER 10000
            DAMAGE ATTACKER 10000
        END
    },
    SPELL {
        NAME: "Expelliarmus",
        ACTION: START
            EQUIP DEFENDER _
            AFTER 5 ROUNDS DO
                EQUIP DEFENDER ---o;
            END
        END
    }
]

MR "Harry" LEARN [
    SPELL_NAME(IfInFor)
    SPELL_NAME(IfInIf)
    SPELL_NAME(IfInAfter)
    SPELL_NAME(ForInIf)
    SPELL_NAME(AfterInIf)
    SPELL_NAME(Multiple)
    SPELL_NAME(Pass)
    SPELL_NAME(Nuke)
    SPELL_NAME(Expelliarmus)
]

MRS "Luna" LEARN [
    SPELL_NAME(Pass)
]

MR "Draco" LEARN [
    SPELL_NAME(Pass)
    SPELL_NAME(Jab)
    SPELL_NAME(IfInFor)
    SPELL_NAME(ForInIf)
    SPELL_NAME(AfterInIf)
]

MR "Cedric" LEARN [
    SPELL_NAME(Pass)
    SPELL_NAME(IfInFor)
    SPELL_NAME(Anapneo)
]

MR "Albus Dumbledore" LEARN [
    SPELL_NAME(Jab)
    SPELL_NAME(Pass)
]

#ifdef ERROR_CASE_1
MR "John Doe" LEARN [
    SPELL_NAME(Pass)
]
#endif

#ifdef ERROR_CASE_2
MR "Draco" LEARN [
    SPELL_NAME(Foobar)
]
#endif

#ifdef ERROR_CASE_3
MRS "Luna Lovegood" LEARN [
    SPELL_NAME(AvadaKadavra)
]
#endif

DUEL

END_GAME