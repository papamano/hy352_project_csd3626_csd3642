/*
 * File:   battle.cpp
 * Author: manos
 *
 * Created on December 27, 2019, 8:06 PM
 */

#include "Hogwarts.h"

BEGIN_GAME

    CREATE SPELL {
        NAME: "Expelliarmus",
        ACTION: START
            EQUIP DEFENDER _
            AFTER 2 ROUNDS DO
                EQUIP DEFENDER ---o
            END
        END
    }

    CREATE SPELL {
        NAME: "Sectusempra",
        ACTION: START
            FOR 5 ROUNDS DO
                DAMAGE DEFENDER 8
            END
        END
    }

    CREATE SPELLS[
        SPELL {
            NAME: "Expulso",
            ACTION: START
                DAMAGE DEFENDER 22
            END
        },
        SPELL {
            NAME: "Pass",
            ACTION: START
            END
        }
    ]

    CREATE SPELL {
        NAME: "Anapneo",
        ACTION: START
            HEAL ATTACKER 30
        END
    }

    CREATE WIZARD {
        NAME: "Harry Potter",
        HOUSE: "Gryffindor",
        HP: 100
    }

    CREATE WIZARD {
        NAME: "Draco Malfoy",
        HOUSE: "Slytherin",
        HP: 90
    }

    MR "Harry Potter" LEARN [
        SPELL_NAME(Expelliarmus)
        SPELL_NAME(Sectusempra)
        SPELL_NAME(Expulso)
        SPELL_NAME(Anapneo)
        SPELL_NAME(Pass)
    ]

    MR "Draco Malfoy" LEARN [
        SPELL_NAME(Expelliarmus)
        SPELL_NAME(Expulso)
        SPELL_NAME(Pass)
    ]

    DUEL

END_GAME