#include "Hogwarts.h"

BEGIN_GAME

CREATE WIZARD {
    NAME: "Harry Potter",
    HOUSE: "Gryffindor",
    HP: 100
}

CREATE WIZARDS [
    WIZARD {
        NAME: "Hermione",
        HOUSE: "Gryffindor",
        HP: 110
    }
]

CREATE WIZARDS [
    WIZARD {
        NAME: "Luna",
        HOUSE: "Ravenclaw",
        HP: 90
    },
    WIZARD {
        NAME: "Cedric Diggory",
        HOUSE: "Hufflepuff",
        HP: 100
    },
    WIZARD {
        NAME: "Draco Malfoy",
        HOUSE: "Slytherin",
        HP: 100
    }
]

CREATE WIZARDS [
    WIZARD {
        NAME: "Cho Chang",
        HOUSE: "Ravenclaw",
        HP: 100
    },
    WIZARD {
        NAME: "Minerva McGonagall",
        HOUSE: "Gryffindor",
        HP: 200
    },
    WIZARD {
        NAME: "Lord Voldemort",
        HOUSE: "Slytherin",
        HP: 700
    },
    WIZARD {
        NAME: "Severus Snape",
        HOUSE: "Slytherin",
        HP: 120
    }
]

CREATE WIZARD {
    NAME: "Albus Percival Wulfric Brian Dumbledore",
    HOUSE: "Gryffindor",
    HP: 1000
}

CREATE SPELL {
    NAME: "Crucio",
    ACTION: START
        FOR 10 ROUNDS DO
            DAMAGE DEFENDER 10
        END
    END
}

CREATE SPELL {
    NAME: "Sectusempra",
    ACTION: START
        IF GET_NAME(DEFENDER) != "Severus Snape" DO
            DAMAGE DEFENDER 30
        ELSE
            DAMAGE ATTACKER 20
        END
    END
}

CREATE SPELLS [
    SPELL {
        NAME: "Expulso",
        ACTION: START
            DAMAGE DEFENDER 22
        END
    },
    SPELL {
        NAME: "Expelliarmus",
        ACTION: START
            EQUIP DEFENDER _
            AFTER 2 ROUNDS DO
                EQUIP DEFENDER ---o
            END
        END
    },
    SPELL {
        NAME: "Anapneo",
        ACTION: START
            HEAL ATTACKER 30
        END
    }
]

CREATE SPELLS [
    SPELL {
        NAME: "Stupefy",
        ACTION: START
            EQUIP DEFENDER _
            AFTER 10 ROUNDS DO
                EQUIP DEFENDER ---o
            END
        END
    },
    SPELL {
        NAME: "Alohomora",
        ACTION: START
            FOR 5 ROUNDS DO
                IF AND(GET_HP(DEFENDER) > 10, GET_HP(DEFENDER) < 80) DO
                    DAMAGE DEFENDER 20
                END
            END
        END
    },
    SPELL {
        NAME: "Accio",
        ACTION: START
            IF GET_HP(ATTACKER) <= 30 DO
                IF OR(GET_HOUSE(ATTACKER) == "Gryffindor",
                      GET_HOUSE(ATTACKER) == "Slytherin",
                      GET_HOUSE(ATTACKER) == "Hufflepuff",
                      GET_NAME(ATTACKER) == "Luna",
                      GET_HP(ATTACKER) >= 25)
                DO
                    HEAL ATTACKER 20
                ELSE
                    HEAL ATTACKER 30
                END
            END
        END
    },
    SPELL {
        NAME: "AvadaKedavra",
        ACTION: START
            IF GET_NAME(DEFENDER) == "Lord Voldemort" DO
                DAMAGE DEFENDER 10000
            ELSE_IF GET_NAME(DEFENDER) == "Albus Percival Wulfric Brian Dumbledore" DO
                DAMAGE DEFENDER 1
            ELSE
                /* Drain the oponent's life. Note that this won't work on a
                 * Gryffindor because they have a defence multiplier */
                DAMAGE DEFENDER GET_HP(DEFENDER)
            END
        END
    },
    SPELL {
        NAME: "Imperio",
        ACTION: START
            EQUIP DEFENDER _
            DAMAGE DEFENDER 30
            HEAL ATTACKER 30
            AFTER 10 ROUNDS DO
                EQUIP DEFENDER ---o
            END
        END
    },
    SPELL {
        NAME: "Lumos",
        ACTION: START
            IF NOT(HAS_WAND(DEFENDER)) DO
                HEAL DEFENDER 20
            ELSE_IF AND(GET_HP(DEFENDER) > 10, GET_HP(DEFENDER) < 100, GET_HP(ATTACKER) >= 50) DO
                DAMAGE DEFENDER 30
            END
        END
    }
]

CREATE SPELL {
    NAME: "Obliviate",
    ACTION: START
    END
}

MR "Albus Percival Wulfric Brian Dumbledore" LEARN [
    SPELL_NAME(Obliviate)
    SPELL_NAME(Crucio)
    SPELL_NAME(Sectusempra)
    SPELL_NAME(Expulso)
    SPELL_NAME(Expelliarmus)
    SPELL_NAME(Anapneo)
    SPELL_NAME(Stupefy)
    SPELL_NAME(Alohomora)
    SPELL_NAME(Accio)
    SPELL_NAME(AvadaKedavra)
    SPELL_NAME(Imperio)
    SPELL_NAME(Lumos)
]

MR "Harry Potter" LEARN [
    SPELL_NAME(Obliviate)
    SPELL_NAME(Crucio)
    SPELL_NAME(Sectusempra)
    SPELL_NAME(Expulso)
    SPELL_NAME(Expelliarmus)
    SPELL_NAME(Anapneo)
    SPELL_NAME(Stupefy)
    SPELL_NAME(Alohomora)
    SPELL_NAME(Accio)
    SPELL_NAME(AvadaKedavra)
    SPELL_NAME(Imperio)
    SPELL_NAME(Lumos)
]

MRS "Hermione" LEARN [
    SPELL_NAME(Crucio)
    SPELL_NAME(Obliviate)
    SPELL_NAME(Anapneo)
    SPELL_NAME(Stupefy)
    SPELL_NAME(Alohomora)
]

MRS "Luna" LEARN [
    SPELL_NAME(Obliviate)
    SPELL_NAME(Sectusempra)
    SPELL_NAME(Expulso)
    SPELL_NAME(Expelliarmus)
]

MR "Cedric Diggory" LEARN [
    SPELL_NAME(Alohomora)
    SPELL_NAME(Accio)
    SPELL_NAME(Obliviate)
    SPELL_NAME(Sectusempra)
]

MR "Draco Malfoy" LEARN [
    SPELL_NAME(Crucio)
    SPELL_NAME(Sectusempra)
    SPELL_NAME(Expulso)
    SPELL_NAME(AvadaKedavra)
    SPELL_NAME(Obliviate)
]

MRS "Cho Chang" LEARN [
    SPELL_NAME(Obliviate)
]

MRS "Minerva McGonagall" LEARN [
    SPELL_NAME(Obliviate)
    SPELL_NAME(Crucio)
    SPELL_NAME(Sectusempra)
    SPELL_NAME(Expulso)
    SPELL_NAME(Anapneo)
    SPELL_NAME(Stupefy)
    SPELL_NAME(Accio)
]

MR "Lord Voldemort" LEARN [
    SPELL_NAME(Crucio)
    SPELL_NAME(AvadaKedavra)
    SPELL_NAME(Imperio)
]

MR "Severus Snape" LEARN [
    SPELL_NAME(Obliviate)
    SPELL_NAME(Sectusempra)
    SPELL_NAME(Crucio)
    SPELL_NAME(AvadaKedavra)
    SPELL_NAME(Lumos)
]

DUEL

END_GAME